<x-layout title="Listagem de Séries">
    <a href="/series/create">Adicionar</a>
    <ul>
        @foreach ($series as $serie)
            <li> {{ $serie }} </li>
        @endforeach
    </ul>

    {{-- HOW TO SET PHP VARIABLES IN JS --}}
    <script>
        const series = {{ Js::from($series) }}
    </script>
</x-layout>