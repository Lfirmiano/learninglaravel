<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SeriesController extends Controller
{
    public function index(Request $request) {
        // PEGAR DADO VIA GET
        // return $request->get('id');
        // PARA REDIRECIONAR
        // return redirect('google.com');

        $series = [
            '1',
            '2',
            'Grey\'s anatomy'
        ];

        // RETORNANDO VIEW COM DADOS
        return view('series.index', compact('series'));
    }

    public function create() {
        return view('series.create');
    }
    //
}
