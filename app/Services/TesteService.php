<?php

namespace App\Services;

use function Psy\debug;

class TesteService {
    private $accountName;
    private $domain;
    private $baseUrl;
    // https://api.github.com/users/LFirmiano/repos
    public function __construct()
    {
        $this->accountName = env('GIT_ACCOUNT');
        $this->domain      = env('GIT_DOMAIN');
        $this->baseUrl = "https://api.{$this->domain}.com/users/{$this->accountName}/repos";
    }

    public function request(string $baseUrl, string $method = 'GET', array $fields = []) {
        $curl = curl_init();

        if ($fields) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($fields));
        }

        $headers = [
            "Accept:application/json",
            "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1521.3 Safari/537.36"
        ];

        curl_setopt_array($curl, [
            CURLOPT_URL => $baseUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_HTTPHEADER => $headers
        ]);

        $responseHeaders = [];
        
        // Captura os headers da resposta
        curl_setopt($curl, CURLOPT_HEADERFUNCTION, function($curl, $header) use (&$responseHeaders) {
            $length = strlen($header);
            $header = explode(':', $header, 2);

            if (count($header) < 2) // ignora headers inválidos
                return $length;

            $responseHeaders[strtolower(trim($header[0]))][] = trim($header[1]);

            return $length;
        });

        $response           = curl_exec($curl);
        $error              = curl_error($curl);
        $responseStatusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        return [$response, $error, $responseStatusCode];
    }

    public function listRepositories() {
        return $this->request($this->baseUrl);
    }
}